cd scripte
echo "---Init terraform----"
terraform init
# terraform validation
echo "---Validate terraform----"
terraform validate
echo "---Plan terraform----"
terraform plan
# lancer le deploiment
echo "---Apply terraform----"
terraform apply -auto-approve
