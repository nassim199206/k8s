#!/bin/bash
echo $KEY >KEY.json
#encode key.json to use in secret
export ENCODED_SECRET=$(base64 < KEY.json | tr -d \\n)
echo "encoded: " $ENCODED_SECRET
gcloud auth activate-service-account $USER --key-file=KEY.json --project=$PROJECT_ID
gcloud container clusters get-credentials $CLUSTER_NAME --zone $ZONE
helm upgrade $RELEASE_NAME $CHART_NAME --set service.port=$PORT \
 --set image.repository=$IMAGE_NAME --set imagePullSecrets.name=$SECRET_NAME \
 --set imagePullSecrets.dockerconfigjson=$ENCODED_SECRET --set namespace=$NAMESPACE \
 --set service.name=$SERVICE_NAME --set deployment.name=$DEPLOYMENT_NAME  --set ingress.name=$INGRESS_NAME\
 --install --namespace $NAMESPACE --create-namespace --wait --debug  --force --atomic
echo $(kubectl get all --namespace=$NAMESPACE)
rm -v KEY.json
