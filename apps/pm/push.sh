
#docker-compose up

gcloud auth configure-docker europe-docker.pkg.dev

export TOKEN=$(gcloud auth print-access-token)
#export  TOKEN=ya29.a0ARrdaM_JVp9_pRFVvdEmTnsAE6geaxVXZuGoC7TpbH6rpXLaT8znC2sXSitKvWnx_9PFO6oz3DhmZtXEFtqsfILTEpV3B7LiBjv3GC5XpahsGKC0Hh2SNxCakoPpGV022BFMtTdYcaT1J18XRtR0XGWzhrwXi2OEAZ8sTw

docker login -u oauth2accesstoken -p $TOKEN  https://europe-docker.pkg.dev


export PROJECT_ID=dsflow
export IMAGE=$1
export SOURCE_IMAGE=$1:$2
export RC=bigapp-rc

docker tag $SOURCE_IMAGE europe-docker.pkg.dev/$PROJECT_ID/$RC/$IMAGE
docker push europe-docker.pkg.dev/$PROJECT_ID/$RC/$IMAGE

# from https://blog.container-solutions.com/using-google-container-registry-with-kubernetes

# kubectl create secret docker-registry pm-registry-secret4 --docker-server=europe-docker.pkg.dev \
  #--docker-username=_json_key \
  #--docker-password="$(cat ./k8s-project-pm-sa3.json)" \
  #--docker-email=70763255677-compute@developer.gserviceaccount.com -n pm-ns

