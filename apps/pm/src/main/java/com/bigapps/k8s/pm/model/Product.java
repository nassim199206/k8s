package com.bigapps.k8s.pm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Product {
    private  Long id;
    private  String name;
    private  String description;
    private  Double price;
}
