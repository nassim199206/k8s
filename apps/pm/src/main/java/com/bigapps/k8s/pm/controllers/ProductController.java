package com.bigapps.k8s.pm.controllers;

import com.bigapps.k8s.pm.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class ProductController {

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    @PostMapping("/product")
    public Product addProduct(@RequestBody Product product){
        logger.info("Recieved product: " + product);
        return product;
    }

}
